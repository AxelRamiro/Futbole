module.exports = function (mongoose) {
  var bcrypt = require('bcrypt-nodejs');
  var Schema = mongoose.Schema;
  var userSchema = Schema({
    avatar: String,
    local: {
      email: String,
      password: String,
    },
    facebook: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    name: String,
    position: String,
    games: [{
      info: {
        type: Schema.Types.ObjectId,
        ref: 'games'
      },
      goals: Number,
      assist: Boolean
    }],
    canPost: Boolean
  }, {
    timestamps: true
  });

  userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  };

  userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
  };

  return mongoose.model('users', userSchema);
}
