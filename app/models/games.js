var autopopulatePlace = function (next) {
  this.populate('place');
  next();
}

module.exports = function (mongoose) {
  var Schema = mongoose.Schema;
  var gameSchema = new Schema({
    players: Number,
    date: String,
    time: String,
    cost: Number,
    creator: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    place: {
      type: Schema.Types.ObjectId,
      ref: 'fields'
    },
    futbolers: [{
      type: Schema.Types.ObjectId,
      ref: 'users'
    }],
    comments: [{
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      },
      date: {
        type: Date,
        default: Date.now
      },
      body: String
    }]
  }, {
    timestamps: true
  });

  gameSchema.pre('find', autopopulatePlace).pre('findOne', autopopulatePlace).pre('findById', autopopulatePlace);

  return mongoose.model('games', gameSchema);
}
