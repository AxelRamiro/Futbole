module.exports = function (mongoose) {
  var FieldSchema = new mongoose.Schema({
    name: String,
    link: String,
    address: String,
    type: String,
    city: String
  });
  return mongoose.model('fields', FieldSchema);
}