'use strict';

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
}

module.exports = function (app, passport, upload, Games, User, Fields, nodemailer) {

  app.get('/', getNextGames, function (req, res) {
    res.render('index', {
      user: req.user,
      games: req.nextGames
    });
  });

  app.get('/login', getNextGames, function (req, res) {
    res.render('login', {
      dest: {
        login: true
      },
      user: req.user,
      games: req.nextGames,
      message: req.flash('loginMessage')
    });
  });

  app.get('/signup', getNextGames, function (req, res) {
    res.render('login', {
      dest: {
        login: false
      },
      user: req.user,
      games: req.nextGames,
      message: req.flash('signupMessage')
    });
  });

  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/welcome', // redirect to the secure profile section
    failureRedirect: '/signup', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
  }));

  app.get('/welcome', isLoggedIn, function (req, res) {
    var email = req.user.local.email;
    nodemailer.mailOptions.to = email;
    nodemailer.mailOptions.subject = "Bienvenido a futbole.mx";
    nodemailer.mailOptions.text = "Hola " + req.user.name + " bienvenido a futbole";
    nodemailer.transporter.sendMail(nodemailer.mailOptions, function (err, info) {
      if (err) {
        console.log(err);
      } else {
        console.log(info.response);
      }
    });
    res.redirect('/profile');
  });

  app.post('/login', passport.authenticate('local-login', {
    successRedirect: '/profile', // redirect to the secure profile section
    failureRedirect: '/login', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
  }));

  app.get('/profile', isLoggedIn, getNextGames, function (req, res) {
    User.findById(req.user._id).populate('games.info').exec(function (err, user) {
      if (user.avatar && user.name && user.position) {
        res.render('profile', {
          user: user,
          games: req.nextGames
        });
      } else {
        req.flash('completeProfile', 'Por favor completa tu información de jugador');
        res.redirect('/profile/edit');
      }
    });
  });

  app.get('/profile/edit', isLoggedIn, function (req, res) {
    User.findById(req.user._id, function (err, user) {
      res.render('edit-profile', {
        user: user,
        message: req.flash('completeProfile')
      });
    });
  });

  app.post('/profile', isLoggedIn, upload.single('avatar'), function (req, res) {
    var file = req.file;
    var body = req.body;
    User.findById(req.user._id, function (err, user) {
      user.name = body.name;
      user.position = body.position;
      if (file) {
        user.avatar = file.filename;
      };
      user.save(function (err, user) {
        if (!err) {
          res.redirect('/profile');
        }
      });
    });
  });

  app.get('/users', isLoggedIn, function (req, res) {
    User.find().select('name local.email position').sort('name').exec(function (err, users) {
      if (!err) {
        res.render('user-list', {
          user: req.user,
          users: users
        });
      }
    });
  });

  app.get('/games/new', isLoggedIn, function (req, res) {
    Fields.find({}, function (err, fields) {
      if (!err) {
        res.render('new-game', {
          user: req.user,
          fields: fields
        });
      }
    });
  });

  app.post('/games/new', isLoggedIn, function (req, res) {
    var newGame = new Games(req.body);
    newGame.creator = req.user._id;
    newGame.save(function (err, game) {
      res.redirect('/profile');
    });
  });

  app.get('/explore', isLoggedIn, function (req, res) {
    Games.find({}).populate('futbolers creator place').exec(function (err, games) {
      if (!err) {
        res.render('explore', {
          user: req.user,
          games: games
        });
      };
    });
  });

  app.get('/jugar-futbol*/:city/:id', isLoggedIn, function (req, res) {
    var joined = false;
    Games.findById(req.params.id, function (err, game) {
      if (game.futbolers.indexOf(req.user._id) !== -1) {
        joined = true;
      }
      game.populate('futbolers creator place').populate({
        path: 'comments.user',
        select: 'name avatar'
      }, function (err, game) {
        res.render('game', {
          user: req.user,
          game: game,
          message: req.flash('join'),
          joined: joined
        });
      });
    });
  });

  app.get('/game/join/:id', isLoggedIn, function (req, res) {
    Games.findById(req.params.id).exec(function (err, game) {
      if (game.futbolers.length < game.players && game.futbolers.indexOf(req.user._id) === -1) {
        game.futbolers.push(req.user._id);
        var currentGame = {
          info: req.params.id,
          goals: 0,
          assist: false
        };
        req.user.games.push(currentGame);
        game.save();
        req.user.save(function (err, user) {
          req.flash('join', "Ya estás apuntado en esta reta, no olvides comentar");
          res.redirect('/jugar-futbol/' + game.place.city + '/' + game._id);
        });
      } else {
        req.flash('join', "Ya estás apuntado en esta reta, no olvides comentar");
        res.redirect('/jugar-futbol/' + game.place.city + '/' + game._id);
      }
    });
  });

  app.get('/game/unjoin/:id', isLoggedIn, function (req, res) {
    Games.findById(req.params.id, function (err, game) {
      var pos = game.futbolers.indexOf(req.user._id);
      game.futbolers.splice(pos, 1);
      var userPos = req.user.games.indexOf(game._id);
      req.user.games.splice(userPos, 1);
      req.user.save();
      game.save(function (err, game) {
        req.flash('join', "Has salido del juego");
        res.redirect('/jugar-futbol/' + game.place.city + '/' + game._id);
      });
    });
  });

  app.post('/comments/:gameId', isLoggedIn, function (req, res) {
    var comment = req.body;
    comment.user = req.user._id;
    Games.findById(req.params.gameId, function (err, game) {
      game.comments.push(comment);
      game.save(function (err, game) {
        var comm = game.comments.pop();
        res.json({
          comment: comm
        });
      });
    });
  });

  // route for facebook authentication and login
  app.get('/auth/facebook', passport.authenticate('facebook', {
    scope: 'email'
  }));

  // handle the callback after facebook has authenticated the user
  app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    successRedirect: '/profile',
    failureRedirect: '/'
  }));

  app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
  });

  function getNextGames(req, res, next) {

    Games.find({}).populate('creator', 'name').populate('futbolers', 'avatar').sort('date').limit(3).exec(function (err, games) {
      if (!err) {
        req.nextGames = games;
      } else {
        req.nextGames = {};
      }
      return next();
    });
  }

};
