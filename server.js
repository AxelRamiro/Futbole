var express = require('express');
var app = express();
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var mongoose = require('mongoose');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var path = require('path');
var configDB = require('./config/database.js');
var flash = require('connect-flash');
var multer = require('multer');
var dirname = process.env.OPENSHIFT_DATA_DIR + '/avatars' || __dirname + '/public/avatars';
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, dirname);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  }
});
var upload = multer({
  storage: storage
});
var nodemailer = require('./config/nodemailer.js');


mongoose.connect(configDB.url);

var Games = require('./app/models/games')(mongoose);
var Users = require('./app/models/user')(mongoose);
var Fields = require('./app/models/field')(mongoose);

//Allow CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
  next();
});

app.use(cookieParser());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');
app.use(session({
  secret: 'jogabonito'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use('/avatars', express.static(process.env.OPENSHIFT_DATA_DIR + '/avatars'));
app.use(express.static(path.join(__dirname, 'public')));
require('./config/passport')(passport, Users);
require('./app/routes.js')(app, passport, upload, Games, Users, Fields, nodemailer);
app.listen(port, ip);
console.log('Server running on port ' + port);
