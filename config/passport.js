var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var configAuth = require('./auth');

module.exports = function (passport, User) {

  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  passport.use('local-signup', new LocalStrategy({
    // Por defecto, local strategy utiliza username y password para autenticar
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, email, password, done) {
    // asynchronous
    // User.findOne wont fire unless data is sent back
    process.nextTick(function () {
      User.findOne({
        'local.email': email
      }, function (err, user) {
        if (err) {
          return done(err);
        }
        // Si existe el usuario se manda un mensaje flash
        if (user) {
          return done(null, false, req.flash('signupMessage', 'El email ya está registrado.'));
        } else {
          // Si no existe lo crea
          var newUser = new User();
          // Crea el usuario con los parámetros del formulario
          newUser.local.email = email;
          newUser.local.password = newUser.generateHash(password);
          newUser.position = req.body.position;
          newUser.name = req.body.name;
          newUser.save(function (err) {
            if (err) {
              throw err;
            }
            return done(null, newUser);
          });
        }

      });
    });
  }));

  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, email, password, done) {
    User.findOne({
      'local.email': email
    }, function (err, user) {
      if (err) {
        return done(err);
      }
      // Si no se encuentra el usuario se envía un mensaje
      if (!user) {
        return done(null, false, req.flash('loginMessage', 'Usuario no registrado.')); // req.flash is the way to set flashdata using connect-flash
      }
      // Si se encuentra el usuario pero su contraseña es incorrecta
      if (!user.validPassword(password)) {
        return done(null, false, req.flash('loginMessage', 'Oops! Contraseña incorrecta.'));
      }
      // Si todo sale bien regresa al usuario
      return done(null, user);
    });

  }));

  passport.use(new FacebookStrategy({
    clientID: configAuth.facebookAuth.clientID,
    clientSecret: configAuth.facebookAuth.clientSecret,
    callbackURL: configAuth.facebookAuth.callbackURL,
    profileFields: ['id', 'displayName', 'emails', 'photos']
  }, function (token, refreshToken, profile, done) {
    console.log(profile);
    process.nextTick(function () {
      User.findOne({
        'facebook.id': profile.id
      }, function (err, user) {
        if (err) {
          return done(err);
        }
        if (user) {
          return done(null, user); // user found, return that user
        } else {
          // if there is no user found with that facebook id, create them
          var newUser = new User();
          // set all of the facebook information in our user model
          newUser.facebook.id = profile.id; // set the users facebook id                   
          newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
          newUser.facebook.name = profile.displayName; // look at the passport user profile to see how names are returned
          newUser.name = profile.displayName;
          newUser.local.email = profile.emails[0].value;
          newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
          // save our user to the database
          newUser.save(function (err) {
            if (err) {
              throw err;
            }
            // if successful, return the new user
            return done(null, newUser);
          });
        }
      });
    });
  }));
};
