var nodemailer = require('nodemailer');

module.exports = {
  transporter: nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: 'futbole.app@gmail.com',
      pass: 'futbolepass'
    }
  }),
  mailOptions: {
    from: 'futbole.app@gmail.com'
  }
}
